/*
    Create functions which can manipulate our arrays.
*/

let registeredUsers = [

    "James Jeffries",
    "Gunther Smith",
    "Macie West",
    "Michelle Queen",
    "Shane Miguelito",
    "Fernando Dela Cruz",
    "Akiko Yukihime"
];

let friendsList = [];

/*
    
   1. Create a function which will allow us to register into the registeredUsers list.
        - this function should be able to receive a string.
        - determine if the input username already exists in our registeredUsers array.
            -if it is, show an alert window with the following message:
                "Registration failed. Username already exists!"
            -if it is not, add the new username into the registeredUsers array and show an alert:
                "Thank you for registering!"
        - invoke and register a new user.
        - outside the function log the registeredUsers array.

*/ 


function registerNew(add){
    let newUsers = registeredUsers.includes(add);
   if (newUsers === true){
    alert("Existing user, registration failed")
   }else {
    registeredUsers[registeredUsers.length] = add;
    alert("Thank you for registrating")
   }

}
     







/*
    2. Create a function which will allow us to add a registered user into our friends list.
        - this function should be able to receive a string.
        - determine if the input username exists in our registeredUsers array.
            - if it is, add the foundUser in our friendList array.
                    -Then show an alert with the following message:
                        - "You have added <registeredUser> as a friend!"
            - if it is not, show an alert window with the following message:
                - "User not found."
        - invoke the function and add a registered user in your friendsList.
        - Outside the function log the friendsList array in the console.

*/


function newFriends(addNewFriends){
    let myNewFriends = registeredUsers.includes(addNewFriends);
   if (myNewFriends === true){
        friendsList.push(argument);
    alert("you have added" + addNewFriends + " as a friend")
   }else {
    registerNew[registerNew.length] = addNewFriends;
    alert("User not found")
   }

}




/*
    3. Create a function which will allow us to show/display the items in the friendList one by one on our console.
        - If the friendsList is empty show an alert: 
            - "You currently have 0 friends. Add one first."
        - Invoke the function.

*/
 function displayFriendList(){
        
        if(friendsList.length == 0){
            alert('You currently have 0 friends. Add one first.' );
        }else{
            friendsList.forEach(function(friend){
                console.log(friend);
            })
        }
    }


/*
    4. Create a function which will display the amount of registered users in your friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - If the friendsList is not empty show an alert:
            - "You currently have <numberOfFriends> friends."
        - Invoke the function

*/
function displayNumberFriendsList(){
    let wholeFriends = friendsList.length
    if (wholeFriends == 0){
        alert('You have 0 friends');
    } else {
        alert('You currently have' + wholeFriends + 'Friend/s');
    }
}

/*
    5. Create a function which will delete the last registeredUser you have added in the friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - Invoke the function.
        - Outside the function log the friendsList array.

*/
  function deleteFriendList(nameOfFriend){
        let friendListSize = friendsList.length;
        let getIndex = friendsList.indexOf(nameOfFriend);
        if(friendListSize == 0){
            alert('You currently have 0 friends. Add one first.' );
        }else{
            friendsList.splice(getIndex,1);
        }
    }


/*
    Stretch Goal:

    Instead of only deleting the last registered user in the friendsList delete a specific user instead.
        -You may get the user's index.
        -Then delete the specific user with splice().

*/






